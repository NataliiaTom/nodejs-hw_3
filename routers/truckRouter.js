const express = require('express');
const router = express.Router();
const { authentificateJWT } = require('./middlewears/authJWT');

const { validateRole } = require('./middlewears/validatRole');
const{createTrucks,getTrucks,getTruckbyId,updateTruckbyId,deleteTruckbyId,assignTruckbyId} = require('../controllers/truckController')

router.post('/', authentificateJWT, validateRole('DRIVER'),createTrucks);
router.get('/', authentificateJWT, validateRole('DRIVER'),getTrucks);
router.get('/:id', authentificateJWT, validateRole('DRIVER'),getTruckbyId);
router.put('/:id', authentificateJWT, validateRole('DRIVER'),updateTruckbyId);
router.delete('/:id', authentificateJWT, validateRole('DRIVER'),deleteTruckbyId);
router.post('/:id/assign', authentificateJWT, validateRole('DRIVER'),assignTruckbyId);

module.exports = router;
