const Joi = require('joi');

module.exports.validatRegistration = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.any().required(),
        password: Joi.any().required(),
        role:Joi.string()
      });

      const {error} = schema.validate(req.body);


  if (error) {
    res.status(400).json({message: error.details[0].message});
  } else {
    next();
  }
}