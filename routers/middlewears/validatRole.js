
module.exports.validateRole = (role) => async (req, res, next) => {
  const user = req.user;
  if (user.role === role) {
    next()
  } else {
      res.status(400).json({message: "Access denied"});
  }
};
