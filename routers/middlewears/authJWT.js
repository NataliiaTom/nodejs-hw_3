const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

module.exports.authentificateJWT=(req,res,next)=>{
    const authorization = req.headers.authorization;    
    if (authorization) {
        const [tokenType, tokenValue] = authorization.split(' ');
        
        // console.log('tokenType '+tokenType)
        // console.log('tokenValue '+tokenValue)
        // console.log('verification'+jwt.verify(tokenValue, JWT_SECRET))
        if (tokenType === 'JWT' && tokenValue) {
          jwt.verify(tokenValue, JWT_SECRET, (err, user) => {
            if (err) {
              return res.sendStatus(403);
            }
            req.user = user;
            next();
          });
        } else {
          res.sendStatus(401);
        }
      } else {
        res.sendStatus(401);
      }
}