const express = require('express');
const router = express.Router();

const{getUser,deleteUser,changePassword}=require('../controllers/userController')
const {authentificateJWT}=require('../routers/middlewears/authJWT')

router.get('/me',authentificateJWT,getUser)
router.delete('/me',authentificateJWT,deleteUser)
router.patch('/me/password',authentificateJWT,changePassword)


module.exports = router;