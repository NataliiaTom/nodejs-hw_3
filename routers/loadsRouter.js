const express = require('express');
const router = express.Router();

const { authentificateJWT } = require('./middlewears/authJWT');
const { validateRole } = require('./middlewears/validatRole');
const {addLoad,postLoad,getLoads,getActiveLoads,nextStateLoads,getLoadbyId,updateLoadbyId,deleteLoadbyId,shippingInfoBybyId}=require('../controllers/loadsController')

router.post('/',authentificateJWT, validateRole('SHIPPER'),addLoad);
router.post('/:id/post',authentificateJWT, validateRole('SHIPPER'),postLoad);
router.get('/',authentificateJWT, getLoads);
router.get('/active',authentificateJWT,validateRole('DRIVER'), getActiveLoads);
router.patch('/active/state',authentificateJWT,validateRole('DRIVER'), nextStateLoads);
router.get('/:id',authentificateJWT, getLoadbyId);
router.put('/:id',authentificateJWT,validateRole('SHIPPER'), updateLoadbyId);
router.delete('/:id',authentificateJWT,validateRole('SHIPPER'), deleteLoadbyId);
router.get('/:id/shipping_info',authentificateJWT,validateRole('SHIPPER'), shippingInfoBybyId);


module.exports = router;