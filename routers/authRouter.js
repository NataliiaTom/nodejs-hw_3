const express = require('express');
const router = express.Router();

const {authRegistration,loginUser,forgotPassword}=require('../controllers/authController')
const {validatRegistration}= require('./middlewears/validateMiddlewear')


router.post('/register',validatRegistration,authRegistration)
router.post('/login',loginUser)
router.post('/forgot_password',forgotPassword)

module.exports = router;