const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');
const mongoose = require('mongoose');
const { getUser } = require('./userController');

const tracksList = [
  { type: 'SPRINTER', width: 300, length: 250, height: 170, payload: 1700 },
  {
    type: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];

module.exports.addLoad = async (req, res) => {
  try {
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    } = req.body;
    const user = req.user;
    if (!user) {
      return res.status(400).json({ message: `There is no proper user` });
    }
    const newLoad = new Load({
      // TODO: Here!!!
      // _id: user._id,
      created_by: user._id,
      status: 'NEW',
      //   state: 'En route to Pick Up',
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      delivery_address: delivery_address,
      dimensions: dimensions,
    });
    await newLoad.save();
    res.status(200).json({ message: 'Load created successfully' });
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.postLoad = async (req, res) => {
  try {
    const id = req.params.id;

    const requestedLoad = await Load.findOne({ _id: id });
    requestedLoad.status = 'POSTED';
    // await requestedLoad.save();
    suitableTypes = tracksList
      .filter((t) => {
        const { width, length, height } = requestedLoad.dimensions;
        return (
          t.payload >= requestedLoad.payload &&
          t.width >= width &&
          t.length >= length &&
          t.height >= height
        );
      })
      .map((t) => t.type);

    //   requestedLoad.save();
    const neededTruck = await Truck.findOne({
      status: 'IS',
      type: { $in: suitableTypes },
    });

    //  console.log(neededTruck.toObject());
    if (neededTruck) {
      neededTruck.status = 'OL';
      await neededTruck.save();
      requestedLoad.status = 'ASSIGNED';
      requestedLoad.assigned_to = neededTruck.assigned_to;
      requestedLoad.state = 'En route to Pick Up';
      requestedLoad.logs.push({
        message: `Load assigned to driver with id ${neededTruck.assigned_to}`,
        time: new Date().toJSON(),
      });
      // requestedLoad.logs.select('-__id');
      await requestedLoad.save();
      res
        .status(200)
        .json({ message: 'Load posted successfully', driver_found: true });
    } else {
      requestedLoad.status = 'NEW';
      // requestedLoad.state="";
      requestedLoad.logs.push({
        message: 'There is no proper truck available',
        time: new Date().toJSON(),
      });
      await requestedLoad.save();
    }
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

const getLimit = (lim) => {
  if (lim) {
    return lim <= 50 ? parseInt(lim) : 50;
  } else {
    return 10;
  }
};

const getStatuses = (user, queryStatus) => {
  if (user.role === 'DRIVER') {
    if (
      queryStatus &&
      (queryStatus == 'SHIPPED' || queryStatus == 'ASSIGNED')
    ) {
      return [queryStatus];
    } else {
      return ['SHIPPED', 'ASSIGNED'];
    }
  } else {
    return queryStatus ? [queryStatus] : [];
  }
};

module.exports.getLoads = async (req, res) => {
  const user = req.user;
  const { limit, offset, status } = req.query;
  const lim = getLimit(limit);
  const off = offset ? parseInt(offset) : 0;
  try {
    const statuses = getStatuses(user, status);
    if (user.role === 'SHIPPER') {
      const loads =
        statuses.length == 0
          ? await Load.find({ created_by: user._id })
              .select('-__v')
              .skip(off)
              .limit(lim)
          : await Load.find({ created_by: user._id, status: { $in: statuses } })
              .select('-__v')
              .skip(off)
              .limit(lim);
      // console.log(createdLoads)
      res.status(200).json({ loads: loads });
    } else {
      const loads =
        statuses.length == 0
          ? await Load.find({ assigned_to: user._id })
              .select('-__v')
              .skip(off)
              .limit(lim)
          : await Load.find({
              assigned_to: user._id,
              status: { $in: statuses },
            })
              .select('-__v')
              .skip(off)
              .limit(lim);
      res.status(200).json({ loads: loads });
    }
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.getActiveLoads = async (req, res) => {
  try {
    user = req.user;
    const states = [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ];
    const activeLoads = await Load.findOne({
      assigned_to: user._id,
      state: { $in: states },
      status: { $ne: 'SHIPPED' }
    });
    console.log(activeLoads)

    res.status(200).json({ load: activeLoads });
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};
module.exports.nextStateLoads = async (req, res) => {
  try {
    const stateByState = {
      'En route to Pick Up': 'Arrived to Pick Up',
      'Arrived to Pick Up': 'En route to delivery',
      'En route to delivery': 'Arrived to delivery',
      'Arrived to delivery': 'Arrived to delivery',
    };
    user = req.user;
    const activeLoad = await Load.findOne({ assigned_to: user._id });

    // if (activeLoad.state == 'Arrived to delivery') {
    //   activeLoad.status = 'SHIPPED';
    //   activeLoad.save();
    //   res.status(200).json({
    //     message: `Load state changed to '${activeLoad.state}'`,
    //   });
    // } else {
      const nextState = stateByState[activeLoad.state];
      activeLoad.state = nextState;
      if (nextState  == 'Arrived to delivery') {
        activeLoad.status = 'SHIPPED';
      }
      await activeLoad.save();
      res.status(200).json({
        message: `Load state changed to '${nextState}'`,
      });
    // }
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.getLoadbyId = async (req, res) => {
  try {
    id = req.params.id;

    const userLoad = await Load.findOne({ _id: id });
    res.status(200).json({ load: userLoad });
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.updateLoadbyId = async (req, res) => {
  try {
    id = req.params.id;
    const data = req.body;

    const checkIfAssigned = await Load.findOne({ _id: id });

    if (checkIfAssigned.assigned_to) {
      res.status(400).json({ message: 'Modification is not possibe' });
    }

    const userLoad = await Load.findOneAndUpdate({ _id: id }, data);

    if (userLoad) {
      res.status(200).json({
        message: 'Load details changed successfully',
      });
    } else {
      res.sendStatus(400).json({ message: 'there is no requested load' });
    }
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.deleteLoadbyId = async (req, res) => {
  try {
    id = req.params.id;

    let load = await Load.findOne({ _id: id });

    if (load.assigned_to) {
      res.status(400).json({ message: 'Can not  be deleted' });
    }
    if (!load) {
      res.status(400).json({ message: 'Wrong id indicated' });
    }
    console.log(load);
    await load.delete();
    res.status(200).json({
      message: 'Load deleted successfully',
    });
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.shippingInfoBybyId = async (req, res) => {
  try {
    id = req.params.id;
    let load = await Load.findOne({ _id: id });
    let truck = await Truck.findOne({ created_by: load.assigned_to });
    if (!load) {
      res.status(400).json({ message: 'Wrong id indicated' });
    }
    res.status(200).json({
      load: load,
      truck: truck,
    });
  } catch (error) {
    console.error('error', error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};
