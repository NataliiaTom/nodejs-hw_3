const { Registration } = require('../models/authModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const generator = require('generate-password');

const { JWT_SECRET } = require('../config');

module.exports.authRegistration = async (req, res, next) => {
  try {
    const { email, password, role } = req.body;
    const pass = await bcrypt.hash(password, 10);

    const user = await new Registration({
      email,
      password: pass,
      role,
    });
    await user.save();
    res.status(200).json({ message: 'Profile created successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Server error' });
  }
};
module.exports.loginUser = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const user = await Registration.findOne({ email });
    console.log(user);
    if (!user) {
      return res
        .status(400)
        .json({ message: `There is no user with such ${username}` });
    }
    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({ message: `Wrong password` });
    }

    const token = jwt.sign({ email: user.email, _id: user._id, role: user.role}, JWT_SECRET);

    res.json({ jwt_token: token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: `Server error` });
  }
};
module.exports.forgotPassword = async (req, res, next) => {
  try {
    const { email } = req.body;

    let foundUser = await Registration.findOne({ email });
    console.log(foundUser);
    if (!foundUser) {
      res.status(400).json({ message: 'User does not exist' });
    }
    let generatedpassword = generator.generate({
      length: 10,
      numbers: true,
    });

    const newPassword = await bcrypt.hash(generatedpassword, 10);

    foundUser.password = newPassword;
    await foundUser.save();
    console.log(foundUser);
    res
      .status(200)
      .json({ message: 'New password sent to your email address' });
  } catch (error) {
    res.status(500).json({ message: `Server error` });
  }
};
