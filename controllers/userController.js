const {Registration}= require('../models/authModel');
const bcrypt = require('bcrypt');

module.exports.getUser=async(req,res)=>{
    const user = req.user;
    console.log(user)
    try{
        const foundUser = await Registration.findOne({_id: user._id});
        if (foundUser) {
            res.status(200).json({
              user: {
                _id: foundUser._id,
                email: foundUser.email,
                created_date: foundUser.created_date,
              },
            });
    }else {
        res.status(400).json({message: 'No user found'});
      }
    } catch (error) {
      res.status(500).json({message: 'Server error'});
    }


}
module.exports.deleteUser=async(req,res)=>{

    const user = req.user;
  try {
    const foundUser = await Registration.findOne({_id: user._id});
    if (foundUser) {
      await foundUser.delete();
      res.status(200).json({
        message: "Profile deleted successfully",
      });
    } else {
      res.status(400).json({message: 'No user found'});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
}
module.exports.changePassword= async(req,res)=>{
    const user = req.user;
    const {oldPassword, newPassword} = req.body;
    const activeUser = await Registration.findOne({_id: user._id});
    console.log("1");
   
    try {
      if (await bcrypt.compare(oldPassword, activeUser.password)) {
        console.log("2");
        activeUser.password = await bcrypt.hash(newPassword, 10);
        console.log("3");
        await activeUser.save();
        console.log("4");
        return res.status(200).json({
          message: 'Password changed successfully',
        });
      } else {
        return res.status(400).json({
          message: 'Failed',
        });
      }
    } catch (error) {
      res.status(500).json({message: 'Server error'});
    }
}