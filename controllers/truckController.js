const { Registration } = require('../models/authModel');
const { Truck } = require('../models/truckModel');
const mongoose = require('mongoose');

module.exports.createTrucks = async (req, res) => {
  try {
    const user = req.user;
    console.log(user);
    if (!user) {
      return res.status(400).json({ message: `There is no proper user` });
    }
    const { type } = req.body;

    const truck = new Truck({
      created_by: user._id,
      // assigned_to:null,
      type: type,
      status: 'IS',
    });

    await truck.save();

    res.status(200).json({ message: 'Truck created successfully' });
  } catch (error) {
      console.error("error", error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
};
module.exports.getTrucks=async (req,res)=>{
try{
  console.log(req)
    const user = req.user;
    
    const foundTrucks = await Truck.find({created_by: user._id}).select('-__v');
    console.log(foundTrucks)
    if(!foundTrucks){
      res.status(400).json({ message: 'There are no trucks yet' });
    }
    res.status(200).json({
      trucks:foundTrucks
    })

} catch (error) {
      console.error("error", error);
    if (error instanceof mongoose.Error.ValidationError) {
      res.status(400).json({ message: 'Validation error' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
}

module.exports.getTruckbyId=async(req,res)=>{
  try{
    user=req.user;
    const id = req.params.id;
    
    const requestedTruck = await Truck.findOne({_id: id}).select('-__v');
    if (!requestedTruck) {
      res.status(400).json({message: 'Wrong id indicated'});
    }

    res.status(200).json({truck:requestedTruck})
  }catch (error) {
    console.error("error", error);
  if (error instanceof mongoose.Error.ValidationError) {
    res.status(400).json({ message: 'Validation error' });
  } else {
    res.status(500).json({ message: 'Server error' });
  }
}


}

module.exports.updateTruckbyId=async(req,res)=>{
  id=req.params.id;
    const {type}=req.body;
  try{
    const requestedTruck= await Truck.findOne({_id: id}).select('-__v');
    if (!requestedTruck) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    requestedTruck.type=type;
   await requestedTruck.save()
    res.status(200).json({message:"Truck details changed successfully"})
  }catch (error) {
    console.error("error", error);
  if (error instanceof mongoose.Error.ValidationError) {
    res.status(400).json({ message: 'Validation error' });
  } else {
    res.status(500).json({ message: 'Server error' });
  }
}
}

module.exports.deleteTruckbyId=async(req,res)=>{
  try{
    id=req.params.id;

    const requestedTruck= await Truck.findOne({_id:id});
    if (!requestedTruck) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    await requestedTruck.deleteOne();
    // await requestedTruck.save();
    res.status(200).json({message:"Truck deleted successfully"})
  }catch (error) {
    console.error("error", error);
  if (error instanceof mongoose.Error.ValidationError) {
    res.status(400).json({ message: 'Validation error' });
  } else {
    res.status(500).json({ message: 'Server error' });
  }
}


}


module.exports.assignTruckbyId=async(req,res)=>{
  try{
    const user= req.user;
    const id=req.params.id;
    const requestedTruck= await Truck.findOne({_id:id});
    if (!requestedTruck) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    requestedTruck.assigned_to=user._id;
    await requestedTruck.save();
    res.status(200).json({message:"Truck assigned successfully"})
  }  catch (error) {
    console.error("error", error);
  if (error instanceof mongoose.Error.ValidationError) {
    res.status(400).json({ message: 'Validation error' });
  } else {
    res.status(500).json({ message: 'Server error' });
  }
}
  
}