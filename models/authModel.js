const mongoose = require('mongoose');


const registrationSchema = mongoose.Schema({
  email: {
    require: true,
    type: String,
    unique: true,
  },
  password: {
    require: true,
    type: String,
    unique: true,
  },
  role: {
    require: true,
    type: String,
    enum:['SHIPPER','DRIVER']
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Registration = mongoose.model('Registration', registrationSchema);
