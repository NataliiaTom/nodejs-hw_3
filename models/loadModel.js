const mongoose = require('mongoose');

const loadsSchema = mongoose.Schema({
  created_by: {
    type: String,
    require: true,
  },
  assigned_to: {
    type: String,
    // require:true,
  },
  status: {
    type: String,
    require: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    // require: true,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
  },
  name: {
    type: String,
    require: true,
  },
  payload: {
    type: Number,
    require: true,
  },
  pickup_address: {
    type: String,
    require: true,
  },
  delivery_address: {
    type: String,
    require: true,
  },
  dimensions: {
    width: {
      type: Number,
      require: true,
    },
    length: {
      type: Number,
      require: true,
    },
    height: {
      type: Number,
      require: true,
    },
  },
  logs: [Object],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadsSchema);
