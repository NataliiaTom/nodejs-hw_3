const mongoose = require('mongoose');


const truckSchema= mongoose.Schema({
    created_by:{
        type:String,
        require:true,
    },
    assigned_to:{
        type:String,
        require: true
    },
    type:{
        type:String,
        require:true,
        enum:['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
    },
    status:{
        type:String,
        require:true,
        enum:['OL', 'IS'],
        
    },
    created_date:{
        type: Date,
    default: Date.now(),
    }
})

module.exports.Truck= mongoose.model('Truck',truckSchema)