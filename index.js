const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');


const authRouter = require('./routers/authRouter');
const userRouter= require('./routers/userRouter')
const truckRouter=require('./routers/truckRouter')
const loadsRouter=require('./routers/loadsRouter')

app.use(express.json());


app.use(morgan('tiny'));
app.use('/api/auth',authRouter)
app.use('/api/users',userRouter)
app.use('/api/trucks',truckRouter)
app.use('/api/truck',truckRouter)
app.use('/api/loads',loadsRouter)

const start = async () => {
    await mongoose.connect(`mongodb+srv://nataliia:1q2w3e4r@cluster0.uvyjy.mongodb.net/test`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(8080, () =>
    console.log(`Server has been started on port 8080`),
    );
};
  start();
  